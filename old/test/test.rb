#encoding: utf-8

require 'pry'
require "test/unit"
require_relative "../lib/memory"
require_relative "../lib/neurone"

class TestNeuralNetwork < Test::Unit::TestCase

  def test_simple_double_neurone
    danger = ->(b, o, i) {i > 0 ? 1 : 0}
    fuir = ->(b, o) {b.l[:danger].output(o)}
    n_danger = NN::Neurone.new(:danger, {forward: danger})
    n_fuir = NN::Neurone.new(:fuir, {forward: fuir, links: [n_danger]})
    n_danger.input("elephant", 1)
    n_danger.input("lion", 3)
    n_danger.input("tasse", 0)
    n_fuir.input("elephant")
    n_fuir.input("lion")
    n_fuir.input("tasse")
    assert(n_fuir.output("elephant") == 1)
    assert(n_fuir.output("lion") == 1)
    assert(n_fuir.output("tasse") == 0)
  end

end
